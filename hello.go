package main

import (
	"fmt"

	"gitlab.com/micahwalter/go-string"
)

func main() {
	fmt.Printf(string.Reverse("hello, world"))
}
